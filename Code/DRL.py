# deep reinforcement learning, hooray!
import numpy as np
from copy import deepcopy
import tensorflow as tf
from tensorflow import keras
import keras
from keras.models import load_model
import h5py

# ToDo
# game of tic tac toe
# neural network to evaluate
# opponent to self play


class TicTacToe:
    def __init__(self):
        self.board = np.array([[2,2,2],[2,2,2],[2,2,2]])
        self.currentPlayer = 0
        self.isWon = False
        self.isWonBy = 0

    def printBoard(self):
        printBoard = np.array([["","",""],["","",""],["","","_"]])
        for i, x in enumerate(self.board):
            for j, y in enumerate(self.board):

                if self.board[i][j] == 0:
                  printBoard[i][j] = "0"

                if self.board[i][j] == 1:
                  printBoard[i][j] = "X"
        print(printBoard)

    def toss(self):
        self.currentPlayer = np.random.randint(0, 2)

    def makeMove(self, x, y):
        if self.currentPlayer == 0:
            self.board[x][y] = 0
            self.currentPlayer = 1
        else:
            self.board[x][y] = 1
            self.currentPlayer = 0
        self.checkForWin()
    def checkForWin(self):

        #horizontal
        for i in range(3):
            if self.board[i][0] == self.board[i][1] and self.board[i][1] == self.board[i][2] and self.board[i][0] != 2:
                self.isWon = True
                self.isWonBy = self.board[i][0]
        #vertikal
        for i in range(3):
            if self.board[0][i] == self.board[1][i] and self.board[1][i] == self.board[2][i] and self.board[0][i] != 2:
                self.isWon = True
                self.isWonBy = self.board[i][0]
        #diagonal
        if self.board[0][0] == self.board[1][1] and self.board[1][1] == self.board[2][2] and self.board[0][0] != 2:
            self.isWon = True
            self.isWonBy = self.board[i][0]
        if self.board[0][2] == self.board[1][1] and self.board[1][1] == self.board[2][0] and self.board[0][2] != 2:
            self.isWon = True
            self.isWonBy = self.board[0][2]
        #draw
        if self.generateLegalMoves().__len__() == 0 and self.isWon is False:
            self.isWon = True
            self.isWonBy = 2
  #additions for RL
    def generateLegalMoves(self):
        legalMoves = {}
        for i in range(3):
            for j in range(3):
                if self.board[i][j] == 2:
                    testboard = deepcopy(self.board)
                    testboard[i][j] = self.currentPlayer
                    legalMoves[(i, j)] = testboard.flatten()
        return legalMoves


    def evaluate(self, model):
        legalMoves = self.generateLegalMoves()
        tracker = {}
        for x in self.generateLegalMoves():
            score = model.predict(legalMoves[x].reshape(1, -1))
            tracker[x] = score
            #print(legalMoves[x].reshape(3, 3))
            #print(score)
        selection = max(tracker, key=tracker.get)
        return selection, legalMoves[selection], tracker[selection][0]

def trainModel(model, steps):
    #todo
    #play against other ai
    wins = 0
    losses = 0
    for i in range(steps):
        game = TicTacToe()
        game.toss()
        boardStates = []
        boardScores = []
        while game.isWon == False:
            if game.currentPlayer == 0:
                data = game.evaluate(model)
                game.makeMove(data[0][0], data[0][1])
                #safe board + score
                boardStates.append(data[1])
                boardScores.append(data[2])
            else:
                data = game.evaluate(model)
                game.makeMove(data[0][0], data[0][1])
                boardStates.append(data[1])
                boardScores.append(data[2])
                #moves = list(game.generateLegalMoves().keys())
                #move = moves[np.random.randint(0, moves.__len__())]
                #game.makeMove(move[0], move[1])
                #boardStates.append(deepcopy(game.board).flatten())
                #boardScores.append(model.predict(game.board.flatten().reshape(1, -1)))

        if game.isWonBy == 0:
            np.roll(boardScores, 1)
            boardScores[boardScores.__len__()-1] = 1
            wins = wins + 1
        if game.isWonBy == 1:
            np.roll(boardScores, 1)
            boardScores[boardScores.__len__()-1] = -1
            losses = losses + 1
        if game.isWonBy == 2:
            np.roll(boardScores, 1)
            boardScores[boardScores.__len__()-1] = 0
        #print(boardStates)
        #print(boardScores)
        boardStates = np.asarray(boardStates)
        boardScores = np.asarray(boardScores)
        #boardStates = (boardStates.min())/(boardStates.max()-boardStates.min())
        #boardScores = (boardScores.min())/(boardScores.max()-boardScores.min())
        #print(boardStates)
        #print(boardScores)
        model.fit(boardStates, boardScores, verbose=0, epochs=1, batch_size=1)
    print("Total Games ", steps)
    print("Wins ", wins)
    print("Losses ", losses)
    print("Draws", steps-wins-losses)

    model.save('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/DLR.h5')


def playGame(model):
    game = TicTacToe()
    game.toss()
    #print(game.printBoard())
    while game.isWon == False:
        if game.currentPlayer == 0:
            print("AIs turn")
            move = game.evaluate(model)
            game.makeMove(move[0][0], move[0][1])
            #moves = list(game.generateLegalMoves().keys())
            #move = moves[np.random.randint(0, moves.__len__())]
            #game.makeMove(move[0], move[1])
            game.printBoard()
        else:
            print("Your move")
            x = input("Your Move? ")
            game.makeMove(int(x[0]), int(x[1]))
            game.printBoard()
    if game.isWonBy == 2:
        print("Draw")
    else:
        print("Player ", game.isWonBy, "has won")


def createmodel():
    model = keras.Sequential([
        keras.layers.Dense(18, input_shape=(9,), kernel_initializer='normal', activation=tf.nn.relu),
        keras.layers.Dense(9, kernel_initializer='normal', activation=tf.nn.relu),
        keras.layers.Dense(1, kernel_initializer='normal')
    ])
    model.compile(optimizer=keras.optimizers.Adam(),
                  loss='mean_squared_error',
                  metrics=['accuracy'])
    return model



model = load_model('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/DLR.h5')
#model = createmodel()
#for i in range(10):
#    trainModel(model, 2500)
#[1, 0, 2, 2, 0, 0, 1, 1, 2] -1
x = np.array([1, 1, 1, 2, 2, 1, 1, 2, 2])
print(model.predict([[x]]))
#playGame(model)

#print(np.array([0, 1, 1, 1, 0, 1, 1, 0, 0]).reshape(3, 3))

