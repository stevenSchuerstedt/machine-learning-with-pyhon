# neuronal network hooray!
import numpy as np
import tensorflow as tf
from tensorflow import keras


class nn:
    def __init__(self, layer1, layer2, layer3):
         self.weightmatrix1 = np.random.rand(layer2, layer1)/100
         self.biasvector1 = np.random.rand(layer2)*10-5
         self.weightmatrix2 = np.random.rand(layer3, layer2) / 100
         self.biasvector2 = np.random.rand(layer3) * 10 - 5

    def calculate_output(self, activationvector):
        return sigma(np.matmul(self.weightmatrix2, sigma(np.matmul(self.weightmatrix1, activationvector) + self.biasvector1)) + self.biasvector2)

    def calculate_single_output(self, activationvector):
        return sigma(np.matmul(self.weightmatrix1, activationvector) + self.biasvector1)

    def calculate_error(self, data, training_data):
        sum = 0
        for i, x in enumerate(data):
            sum += ((np.linalg.norm(data[i]-self.calculate_output(training_data[i])))**2)*1/2*data.size
        return sum

    def update_weightsAndBiases(self, data, labels, lernrate):

        data = flatten(data)
        labels = makevector(labels)
        print(self.calculate_error(labels, data))

        for n, datensatz in enumerate(data):
            ausgabe = self.calculate_output(datensatz)
            soll_ausgabe = labels[n]
            for i, neuron in enumerate(self.weightmatrix2):
                fehler = ausgabe[i] * (1 - ausgabe[i]) * (ausgabe[i] - soll_ausgabe[i])
                deltab = - lernrate * fehler
                self.biasvector2[i] = self.biasvector2[i] + deltab
                for j, weight in enumerate(neuron):
                    deltaw = - lernrate * fehler * datensatz[j]
                    self.weightmatrix2[i][j] = self.weightmatrix2[i][j] + deltaw

            ausgabe_single = self.calculate_single_output(datensatz)
            for i, neuron in enumerate(self.weightmatrix1):
                sumfehler = 0
                for k, zneuron in enumerate(self.weightmatrix2):
                    sumfehler += ausgabe[k] * (1 - ausgabe[k]) * (ausgabe[k] - soll_ausgabe[k]) * self.weightmatrix2[k][i]

                fehler = ausgabe_single[i] * (1 - ausgabe_single[i]) * sumfehler
                deltab = - lernrate * fehler
                self.biasvector1[i] = self.biasvector1[i] + deltab
                for j, weight in enumerate(neuron):
                    deltaw = - lernrate * fehler * datensatz[j]
                    self.weightmatrix1[i][j] = self.weightmatrix1[i][j] + deltaw

        np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix1.txt', self.weightmatrix1)
        np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/biasvector1.txt', self.biasvector1)
        np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix2.txt', self.weightmatrix2)
        np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/biasvector2.txt', self.biasvector2)
        return self




def create_nn(layer1, layer2):
    weightmatrix = np.random.rand(layer2, layer1)
    biasvector = np.random.rand(layer2)
    return weightmatrix, biasvector


def sigma(vector):
    result = np.empty_like(vector)
    for i, x in enumerate(vector):
        result[i] = 1/(1+np.exp(-x))
    return result




def flatten(dataset):
    result = np.zeros([dataset.shape[0], 784])
    for i, x in enumerate(dataset):
        for j, y in enumerate(x):
            for k, z in enumerate(y):
                result[i][k + 28 * j] = z;
    return result


def makevector(train_labels):
    result = np.zeros([train_labels.size, 10])
    for i, x in enumerate(train_labels):
        result[i][x] = 1
    return result

def check(netz, testdaten, ergebnis):
    richtige = 0
    for i, x in enumerate(flatten(testdaten)):
        if np.argmax(netz.calculate_output(x)) == np.argmax(makevector(ergebnis)[i]):
            richtige = richtige + 1

    print(richtige)
    print(testdaten.shape[0])
    return richtige/testdaten.shape[0]


fashion_mnist = keras.datasets.fashion_mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
train_images = train_images / 255.0
test_images = test_images / 255.0

train_images = train_images[10000:30000]
train_labels = train_labels[10000:30000]
n1 = nn(784, 15, 10)
#print(n1.calculate_output(flatten(train_images)[0]))
n1.weightmatrix1 = np.loadtxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix1.txt', dtype=float)
n1.biasvector1 = np.loadtxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/biasvector1.txt', dtype=float)
n1.weightmatrix2 = np.loadtxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix2.txt', dtype=float)
n1.biasvector2 = np.loadtxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/biasvector2.txt', dtype=float)
#for i in range(10000):
#    print(np.argmax(n1.calculate_output(flatten(test_images)[i])), np.argmax(makevector(test_labels)[i]))
#for x in range(1000):
#    n1.update_weightsAndBiases(train_images, train_labels, 1.0)



print(check(n1, test_images, test_labels))
print("done")
#print(n1.calculate_output(flatten(train_images)[4]))
# np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix.txt', n1.weightmatrix)
# np.savetxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/biasvector.txt', n1.biasvector)

# n1.weightmatrix = np.loadtxt('C:/Users/Timmmeee/Desktop/studium/2019/neuronale netze/weightmatrix.txt', dtype=float)


# n1.update_weightsAndBiases(train_images, train_labels, 1)


# print(n1.update_weights(1).weightmatrix)

# print(n1.calculate_output(flatten(train_images)[4]))

# print(n1.calculate_error(makevector(train_labels), flatten(train_images)))
# a = create_nn(784, 10)

# print(calculate_output(a[0], a[1], [0.1, 0.2, 0.3, 0.4, 0.1, 0.1, 0.2, 0.9]))

#classification rate with 1 layer network: 69,31%